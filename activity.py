from abc import ABC


class Animal(ABC):
 # Initialize
    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age

    def eat(food):
        pass

    def make_sound():
        pass

    def call():
        pass


class Dog(Animal):

    # Setters and Getters
    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_breed(self):
        return self.breed

    def set_breed(self, breed):
        self.breed = breed

    def get_age(self):
        return self.age

    def set_age(self, age):
        self.age = age

    # Method

    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print(f"Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self.name}!")


class Cat(Animal):

   # Setters and Getters
    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_breed(self):
        return self.breed

    def set_breed(self, breed):
        self.breed = breed

    def get_age(self):
        return self.age

    def set_age(self, age):
        self.age = age

    # Method

    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print(f"Miaow! Nyaw! Nyaaaaaa!")

    def call(self):
        print(f"{self.name}, come on!")


dog1 = Dog("Isis", "Dalmatian", 15)
dog1_name = dog1.get_name()
dog1_breed = dog1.get_breed()
dog1_age = dog1.get_age()
print(
    f"My name is {dog1_name}, I'm a {dog1_breed} and I'm {dog1_age} years old!")
dog1.eat("Chicken")
dog1.make_sound()
dog1.call()


cat1 = Cat("Puss", "Persian", 4)
cat1.set_name("Kitty")
cat1_name = cat1.get_name()
cat1_breed = cat1.get_breed()
cat1_age = cat1.get_age()
print(
    f"My name is {cat1_name}, I'm a {cat1_breed} and I'm {cat1_age} years old!")
cat1.eat("Tilapia")
cat1.make_sound()
cat1.call()
